package com.kuding.config;

import com.kuding.message.INoticeSendComponent;
import com.kuding.pojos.PromethuesNotice;

public interface NoticeSendContfigure<T extends PromethuesNotice> {

	public INoticeSendComponent<T> configure();
}
